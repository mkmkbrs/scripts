#!/bin/sh

# Download YouTube videos from clipboard URL
# Allows to choose video/audio quality
# Bakes chapters into the video (--add-metadata)

# Requires 'youtube-dl'/'yt-dlp'
# https://github.com/yt-dlp/yt-dlp

dltool=yt-dlp             # download tool
url="$(xsel -op)"         # get clipboard content
dldir="$HOME/Videos/yt/"  # downloads directory

# Create dmenu prompt with options
prompt=$(printf "%s720p [med]\n1080p [high]\naudio only\nplay now" | dmenu -i -p "Download:")

# On user's choice: set video/audio quality to download
quality=''

case "$prompt" in
    "1080p [high]")       quality='137+251';;
    "720p [med]")         quality='22';;
    "audio only")         quality='251';;
    "play now")           sh "$HOME/scripts/dmenu/dmenu-ytwatch.sh"; exit 1;;
    "")                   exit 1 ;;
esac

# Get video title and notify user
title="$($dltool --get-title "$url")"
notify-send 'Downloading...' "${title}"

# youtube-dl and yt-dpl have options to save at specific path
# but require a filename template. Don't bother.
cd "$dldir"

# Download and handle status
"$dltool" -q --no-warnings -f "$quality" "$url" --add-metadata &&
    notify-send 'Download complete' "${title}" ||
    notify-send -u critical 'Download failed' "${title}" &
