#!/bin/sh

. $HOME/scripts/dmenu/dmenu-mount-device.sh
. $HOME/scripts/dmenu/dmenu-mount-sshfs.sh

# Mount devices, sshfs, etc. from dmenu
case "$prompt" in
	"")       exit 1 ;;
	sshfs)    sshfsmnt ;;
	*)        devmnt ;;
esac

