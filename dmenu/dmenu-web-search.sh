#!/bin/sh

# Simple web quicksearch for dmenu

term=urxvt
browser=links

prompt=$(echo "" | dmenu -i -p "Search:")
sengine="https://lite.duckduckgo.com/lite/?q="

case $prompt in
	"") exit 1 ;;
	*) $term -e $browser "$sengine$prompt" & ;;
esac
