#!/bin/sh

# Watch YouTube videos from clipboard URL

dltool=yt-dlp             # download tool
watchtool=mpv             # video player
watchopts="--fullscreen"  # video player options

# Copy url from browser
xdotool key ctrl+l ctrl+c Escape

# Get url from clipboard
url=$(xsel -op)

# Get title of the video
title=$($dltool --get-title "$url")

# Notify and play
notify-send "$watchtool" "Queuing up - $title" && "$watchtool" "$watchopts" "$url"
