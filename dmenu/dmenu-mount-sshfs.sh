#!/bin/sh

server="pi@192.168.0.12"
servermountpoint="/home/pi/"
localmountpoint="$HOME/sshmnt"
ssh_command='ssh -i '$HOME'/.ssh/id_rsa_desktop'

sshfsmnt() {
    (sshfs "$server":"$servermountpoint" "$localmountpoint" -o ssh_command="$ssh_command" &&
        notify-send "SSHFS Mount" "Mounted $servermountpoint at $localmountpoint") ||
        notify-send "SSHFS Mount" "An error occured"
}
