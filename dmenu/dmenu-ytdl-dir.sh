#!/bin/sh

# TODO Remove dead empty dir when the download errors out on a new channel

# Download YouTube videos from clipboard URL
# Allows to choose video/audio quality
# Bakes chapters into the video (--add-metadata)

# Requires 'youtube-dl'/'yt-dlp'
# https://github.com/yt-dlp/yt-dlp

# Copy url from browser
xdotool key ctrl+l ctrl+c Escape

dltool=yt-dlp             # download tool
url="$(xsel -op)"         # get clipboard content
dldir="$HOME/Videos/yt/"  # downloads directory

# Create dmenu prompt with options
prompt=$(printf "%s720p [med]\n1080p [high]\naudio only\nplay now" | dmenu -i -p "Download:")

# On user's choice: set video/audio quality to download
quality=''

case "$prompt" in
    "1080p [high]")       quality='137+251';;
    "720p [med]")         quality='136+251';;
    "audio only")         quality='251';;
    "play now")           sh "$HOME/scripts/dmenu/dmenu-ytwatch.sh"; exit 1;;
    "")                   exit 1 ;;
esac

# Get video title and channel 
title="$($dltool --get-title "$url")"
channel="$($dltool --print uploader "$url")"

# Notify user
notify-send 'Downloading...' "${title}"

# NOTE
# youtube-dl and yt-dpl have options to save at specific path
# but require a filename template. Don't bother.
cd "$dldir" || { notify-send "Couldn't find download directory"; exit 1; }
mkdir -p "$channel"
cd "$channel" || { notify-send "Couldn't find channel directory"; exit 1; }

# Download and handle status
# --add-metadata - Add chapters
"$dltool" -q --no-warnings -f "$quality" "$url" --add-metadata &&
    notify-send 'Download complete' "${title}" ||
    notify-send -u critical 'Download failed' "${title}" &
