#!/bin/sh

# Shutdown for dmenu

# Put cases into dmenu
prompt=$(echo -e "logout\nreboot\nshutdown\n+15\n+30\n-c" | dmenu -i)
echo $prompt

# Act accordingly
case "$prompt" in
	"")       exit 1 ;;
	logout)   killall dwm ;;
	reboot)   reboot ;;
	shutdown) shutdown now ;;
	+15)      shutdown +15 && notify-send "Shutdown" "15 minutes untill shutdown." ;;
	+30)      shutdown +30 && notify-send "Shutdown" "30 minutes untill shutdown." ;;
	-c)       shutdown -c && notify-send "Shutdown" "Shutdown has been canceled." ;;
esac

