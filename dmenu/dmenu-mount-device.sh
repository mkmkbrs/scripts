#!/bin/sh

# FIXME: Prompt is here but also in the main script
# FIXME: Why exclude sda?

# Loop trough all block devices in list excluding 'sda'
# and already mounted at /run/media/ devices
loop=$(for i in $(lsblk -lne11 | grep -v "/run/media/" | awk '/sd[a-z][0-9]/ { print "dev/" $1 "(" $4 ")" }');

do
    echo "${i}"
done)

# Put these devices into dmenu
prompt=$(printf "%s\nsshfs" "$loop" | dmenu -i -p "Mount:")

devmnt () {
    # Reduce PROMPT to device name (e.g. 'sdb')
    mount=$(echo "$prompt" | awk '/sd[a-z][0-9]/' | cut -c 5- | cut -c -4)
    echo "$mount"

    # Mount and notfy
    # (dunst or other notification-deamon is required)
    udisk=$(udisksctl mount -b /dev/"$mount") && notify-send "Mount" "$udisk"
}
