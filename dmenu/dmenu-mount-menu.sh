#!/bin/sh

# Prompt user to mount/unmount the device using dmenu

# Put cases into dmenu
prompt=$(printf "mount\nunmount" | dmenu -i)

# Act accordingly
case "$prompt" in
	"") exit 1 ;;
	mount) sh "$HOME/scripts/dmenu/dmenu-mount.sh" ;;
	unmount) sh "$HOME/scripts/dmenu/dmenu-unmount.sh" ;;
esac

