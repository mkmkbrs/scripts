#!/bin/sh

# Unmount devices/sshfs from dmenu

localmountpoint="$HOME/sshmnt"

# Loop trough all block devices in list excluding
# any device that is not mounted on /run/media
loop=$(for i in $(lsblk -lne11 | awk '$0~v { print "dev/" $1 "(" $4 ")" }' v="/media");

do
	echo "${i}"
done)

# Put these devices into dmenu
prompt=$(printf "%s\nsshfs" "$loop" | dmenu -i -p "Unmount:")

devumnt() {
	# Reduce prompt to device name (e.g. 'sdb')
	unmount=$(printf "%s" "$prompt" | awk '/sd[a-z][0-9]/' | cut -c 5- | cut -c -4)
	
	# Unmount and notfy
	# (dunst or other notification-deamon is required)
	udisk=$(udisksctl unmount -b /dev/"$unmount") && notify-send "Unmount" "$udisk"
}

sshfsumount () {
	fusermount3 -u "$localmountpoint" &&
		notify-send "SSHFS Unmount" "Unmounted $localmountpoint."
}

# Act accordingly
case "$prompt" in
	"")    exit 1 ;;
	sshfs) sshfsumount ;;
	*)     devumnt ;;
esac

