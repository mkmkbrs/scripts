#!/bin/sh

# Start the clock on the second monitor
# upon the X launch

# Requires 'tty-clock'
# https://github.com/xorg62/tty-clock

# Switch to second monitor
xdotool key super+n

# Switch to the fifth tag
xdotool key super+5

# Open new terminal with the clock
urxvt -name tty-clock -e tty-clock -c &

# Wait a little
sleep 0.2

# Switch back to the primary monitor
xdotool key super+n
