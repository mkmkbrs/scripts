#!/bin/sh

# Get the filename
printf '%s' "a.b.c.txt" | rev | cut -d"." -f2- | rev

# Get the extension
printf '%s' "a.b.c.txt" | rev | cut -d"." -f1 | rev
