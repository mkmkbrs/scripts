#!/bin/sh

yesornp() {
	while true; do
		read -p "Yes or no question?" yn
		case $yn in
			[Yy]*) echo "Yes"; break;;
			[Nn]*) echo "No"; break;;
			*) echo "Please answer yes or no.";;
		esac
	done
}

yesornp

