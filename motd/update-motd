#!/bin/sh

# This same script lives in /usr/bin/update-motd
# Execute it at the very end of the /etc/profile

# Colors
# Change colors with the season
MONTH=$(date '+%m')

c_cyan='\033[0;36m'
c_purple='\033[0;35m'
c_green='\033[0;32m'
c_yellow='\033[0;33m'
c_white='\033[0;37m'

# Default - white
c_main=$c_white

set_color() {
    case $MONTH in
        "12"|"01"|"02") c_main=$c_cyan ;;
        "03"|"04"|"05") c_main=$c_purple ;;
        "06"|"07"|"08") c_main=$c_green ;;
        "09"|"10"|"11") c_main=$c_yellow ;;
        "") c_main=$c_white ;;
    esac
}

set_color

# No color
nc='\033[0m'

# Motd variables

# Date
motd_date=$(date '+%A, %B %e')

# Weather
# $WTRLOC is defined in /etc/environment
# Ex.: WTRLOC=Argentina
motd_weather=$(curl -m 3 -s wttr.in/"$WTRLOC"?format="%l:+%C,+%t\n" | cut -f 2 -d ":" | cut -c 2-)

# Bbile quote of the day
# The script is executed as root so use full path instead of $HOME for user's
# files
motd_bqod=$(shuf -n 1 /home/mkmkbrs/scripts/motd/bible.txt | sed 's/\t/ /g' | fold -w 80 -s)

# Last login and dotsync
motd_lastlogin=$(last -a | grep -i logged | awk '{ print $3 " " $4 " " $5 " " $6 " by " $1}')
motd_lastdotsync=$(tail -1 /home/mkmkbrs/log.txt | awk '{ print $1 " at " $2 }')

# OS name
motd_osname=$(grep PRETTY_NAME /etc/os-release | cut -f 2 -d "=" | sed 's/\"//g')

# Host (vendor, family)
get_vendor=$(cat /sys/devices/virtual/dmi/id/sys_vendor)
get_family=$(cat /sys/devices/virtual/dmi/id/product_family)
motd_host=$(printf '%s %s' "$get_vendor" "$get_family")

# Kernel release (release, hardware arch)
motd_kernel=$(uname -rm)

# Installed packages
motd_packages=$(pacman -Qq | wc -l)

# Diskspace (used/total (free))
motd_diskspace_root=$(df -h /root | awk '{ print "Root " $3 "/" $2 " ("$4")" }' | tail -n 1)
motd_diskspace_home=$(df -h /home | awk '{ print "Home " $3 "/" $2 " ("$4")" }' | tail -n 1)

# Uptime
motd_uptime=$(uptime -p | sed 's/up //g')

# CPU Info
motd_cpuinfo=$(grep "model name" /proc/cpuinfo | head -n 1 | cut -f 2 -d ":" | cut -c 2-)

# Message of the day
printf "      /\\
     /  \\      
    /\\   \\      ${c_main}Welcome home!${nc}
   /      \\     Today is $motd_date.
  /   ,,   \\    It's $motd_weather outside.
 /   |  |  -\\
/_-''    ''-_\\

${c_main}Bible quote of the day${nc}
$motd_bqod

${c_main}Last login${nc}    $motd_lastlogin
${c_main}Last dotsync${nc}  $motd_lastdotsync

${c_main}Host${nc}          $motd_host
${c_main}OS${nc}            $motd_osname
${c_main}Kernel${nc}        $motd_kernel
${c_main}Packages${nc}      $motd_packages
${c_main}Diskspace${nc}     $motd_diskspace_root, $motd_diskspace_home
${c_main}Uptime${nc}        $motd_uptime
${c_main}CPU${nc}           $motd_cpuinfo
"
