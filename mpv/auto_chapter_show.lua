-- Automatically display chapter titles on chapter change
-- by SignalCash

local function showChapterTitle()
    local chapterTitle = mp.get_property_osd("chapter-metadata/by-key/title")
    local displayTime = 5
    mp.osd_message(chapterTitle, displayTime)
end

mp.observe_property("chapter", nil, showChapterTitle)
