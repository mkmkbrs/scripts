# MPV scripts

## How to

Make a `scripts` directiory in your `.config/mpv` directiory and symlink the script:

```
ln -sf ~/scripts/mpv/script.lua ~/.config/mpv/scripts/script.lua
```
