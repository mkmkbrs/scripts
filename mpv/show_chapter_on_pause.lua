-- Display chapter title on pause

function on_pause(name, value)
    if value == true then
      local chapterTitle = mp.get_property_osd("chapter")
      local displayTime = 5
      mp.osd_message(chapterTitle, displayTime)
    end
end

mp.observe_property("pause", "bool", on_pause)
