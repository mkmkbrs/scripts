#!/bin/sh

# Display artist, track and title with status icon

if ps -C cmus > /dev/null; then
	remote_call=$(cmus-remote -Q)

	status=$(printf '%s' "$remote_call" | grep '^status')

	artist=$(printf '%s' "$remote_call" |
				 grep '^tag artist' |
				 sed '/^tag artistsort/d' |
				 awk '{gsub("tag artist ", "");print}')

	title=$(printf '%s' "$remote_call" |
				grep '^tag title' |
				sed -e 's/tag title //' |
				awk '{gsub("tag title ", "");print}')

	# Decide status icon
	case $status in
		"status playing")   status_icon="\uE058";;
		"status paused")    status_icon="\uE059";;
		*)                  status_icon="";;
	esac

	# Use file's name as a title if no title metadata is found
	if [ -z "$title" ]; then
		title=$(printf '%s' "$remote_call" | grep '^file' | sed 's|.*/||' | rev | cut -d"." -f2- | rev )
		printf '%b%s\n' "$status_icon" "$title"
	else
		printf '%b%s - %s\n' "$status_icon" "$artist" "$title"
	fi
fi
