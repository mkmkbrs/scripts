#!/bin/sh

# Display the current date and time as follows:
# "Mon, Jan 01  10:00"

printf '%b%s\n' "$icon" "$(date '+%a, %b %d  %H:%M')"
