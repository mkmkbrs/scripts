# Bar

A set of shell scripts for a status bar
([dwmblocks](https://github.com/torrinfail/dwmblocks) primarily, but can be used
with others as well).

## Optional icons

If you wish to use [siji](https://github.com/stark/siji) font to display icons,
pass "1" to the script:

`sh /path/to/script/bar-cputemp.sh 1` 

If no input argument of any kind is given - no icon will be displayed.

__Exceptions are:__

- Weather script (`bar-weather.sh`)
- Cmus Status script (`bar-cmus-status.sh`)
- Some date and time scripts (`bar-time.sh`, `bar-date-time.sh`)

## Weather script

`bar-weather.sh` uses [wttr.in](https://wttr.in) as a source of info and takes
location name as an argument.

`sh /path/to/script/bar-weather.sh Argentina` 

The regular script displays siji font by default but there is an emoji based
version - `bar-weather-emoji.sh`.

## Cmus script

`bar-cmus-status` shows currently playing song, takes no arguments and uses siji
font icons by default.
