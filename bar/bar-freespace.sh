#!/bin/sh

# Display available disk space from specified partition

icon="\uE02A"
[ -z "$1" ] && icon=""

mountpoint="/home"
printf '%b%s\n' "$icon" "$(df -h | grep -m 1 $mountpoint | awk '{print $4}')"
