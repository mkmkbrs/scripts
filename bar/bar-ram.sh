#!/bin/sh

# Display current RAM usage and total amount availabe

icon="\uE028"
[ -z "$1" ] && icon=""

ram() {
	free -h --si | awk '/^Mem:/ {print $3 "/" $2}'
}

printf '%b%s\n' "$icon" "$(ram)"
