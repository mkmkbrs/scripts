#!/bin/sh

# Display current keyboard layout

# Requires 'xkblayout-state'
# https://github.com/nonpop/xkblayout-state

icon="\uE26F"
[ -z "$1" ] && icon=""

printf '%b%s\n' "$icon" "$(xkblayout-state print %s)"
