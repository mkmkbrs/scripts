#!/bin/sh

# Get the weather
# $1 - weather location name passed to the script, ex.: Argentina
# %c - weather emoji
# %t - temperature
wtr=$(curl -m 3 -s https://wttr.in/"$1"?format="%c/%t\n")

# Split the response data between two variables
wtricon=$(echo "$wtr" | cut -d "/" -f1)
wtrtemp=$(echo "$wtr" | cut -d "/" -f2)

# Print out the result
printf '%b%s\n' "$wtricon" "$wtrtemp"
