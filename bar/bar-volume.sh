#!/bin/sh

# Display current volume levels

# 'pamixer' required
# https://github.com/cdemoulins/pamixer

volume=$(pamixer '--get-volume')

if [ "$volume" -gt 50 ]; then
	icon="\uE05D"
	[ -z "$1" ] && icon=""
	printf '%b%s\n' "$icon" "$volume"
elif [ "$volume" = 50 ]; then
	icon="\uE050"
	[ -z "$1" ] && icon=""
	printf '%b%s\n' "$icon" "$volume"
elif [ "$volume" -lt 50 ] && [ "$volume" -ge 10 ]; then
	icon="\uE04E"
	[ -z "$1" ] && icon=""
	printf '%b%s\n' "$icon" "$volume"
else
	icon="\uE04F"
	[ -z "$1" ] && icon=""
	printf '%b%s\n' "$icon" "$volume"
fi
