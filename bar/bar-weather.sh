#!/bin/sh

# Get the weather
# $1 - weather location name passed to the script, ex.: Argentina
# %C - weather type
# %t - temperature
wtr=$(curl -m 3 -s https://wttr.in/"$1"?format="%C/%t\n")

# Split the response data between two variables
wtrtype=$(echo "$wtr" | cut -d "/" -f1)
wtrtemp=$(echo "$wtr" | cut -d "/" -f2)

# Change the weather icon according to the time of the day
curtime=$(date '+%H')

time_check() {
	if [ "$curtime" -ge 21 ] && [ "$curtime" -le 23 ] || [ "$curtime" -le 4 ];
	then
		# Nighttime icon
		wtricon="$1"
	else
		# Daytime icon
		wtricon="$2"
	fi
}

# Set the appropriate icon to the weather type
case $wtrtype in 
	"Sunny") 		  time_check '\uE233' '\uE234';;
	"Clear ") 		  time_check '\uE233' '\uE234';;
	"Mist")                        wtricon='\uE235';;
	"Fog")                         wtricon='\uE235';;
	(*?fog*)                       wtricon='\uE235';;
	"Partly cloudy ")  time_check '\uE232' '\uE231';;
	"Cloudy ")                     wtricon='\uE22B';;
	"Overcast ")                   wtricon='\uE22B';;
	(*?overcast*)                  wtricon='\uE22B';;
	(*?rain*)                      wtricon='\uE230';;
	"Light drizzle")               wtricon='\uE230';;
	"Patchy light drizzle")        wtricon='\uE230';;
	"Thundery outbreaks possible") wtricon='\uE22C';;
	(*?thunder*)                   wtricon='\uE22C';;
	(*?sleet*)                     wtricon='\uE22E';;
	(*?snow*)                      wtricon='\uE22E';;
	(*?freezing*)                  wtricon='\uE22E';;
	"Unknown location;")           wtricon='x';;
	*)                             wtricon='x';;
esac

printf '%b%s\n' "$wtricon" "$wtrtemp"
