#!/bin/sh

# Display current CPU temperature

# The line that 'awk' gets will be different depending on your CPU manufacturer
# Run 'sensors' and adjust the script accordingly

icon="\uE01C"
[ -z "$1" ] && icon=""

cpu() {
	sensors | awk '/^Tctl:/ {print $2}' | cut -c -3 
}

printf '%b%s%s\n' "$icon" "$(cpu)" "°C"
