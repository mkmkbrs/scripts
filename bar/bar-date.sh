#!/bin/sh

# Display current date
# Format: Day of The Week, Month, Day

icon="\uE1CD"
[ -z "$1" ] && icon=""

printf '%b%s\n' "$icon" "$(date '+%a, %b %d')"
