#!/bin/bash
# Make a fullscreen screenshot, ask user for a name
# then display a notification

# Read 'xdg-user-dirs' config file
# source $HOME/.config/user-dirs.dirs

# Set the initial name of the file to format
initial_name='%H:%M-%d-%m-%Y_$wx$h.png'

# Set your screenshots folder either using xdg variables
# or directly (ex. '$HOME/Pictures/screenshots')
scr_dir=$HOME/Pictures/screenshots
# echo $scr_dir

# Create a temporary directory inside user's home
# if it doesn't exists already
tmp_dir=$HOME/tmp
mkdir -p $tmp_dir

# Create temporary directory for scrot files
scr_tmp_dir=$(mktemp -d -t scr-XXXXXXXXXX --tmpdir=$tmp_dir)
# echo $scr_tmp_dir

# Make screenshot and move it into scrot's temporary directory
sleep 1s; scrot -s $initial_name -e 'mv $f '$scr_tmp_dir'' &&

# Capture the screenshot file name
scr_file=$(ls $scr_tmp_dir)
# echo $scr_file

# Call dmenu asking for user's input
PROMPT=$(echo -e "format" | dmenu -i -p "Name the file:")
# echo $PROMPT

# Capture new name of the screenshot
new_name=$(echo $PROMPT)
# echo $new_name

rename_copy_func() {
	# Go into user's screenshot directory
	# and check for similliar names
	cd $scr_dir

	# If the name is similliar,
	# perform dark magic
	if [[ -e $new_name.png || -L $new_name.png ]] ; then
		i=1
		while [[ -e $new_name-$i.png || -L $new_name-$i.png ]] ; do
			let i++
		done
	new_name=$new_name-$i
	fi

	# Get back to temporary screenshot directory
	cd $scr_tmp_dir
}

case "$PROMPT" in
	# Format the name as provided by 'initial_name' variable,
	# then move the file to scr_dir and notify user
	"format") mv $scr_tmp_dir/$scr_file $scr_dir && 
	notify-send 'Scrot' ''$scr_file' has been saved.' ;;

	# Empty option (or ESC) exits out of dmenu without saving the screenshot
	"") ;;

	# * option renames the file if neceserry as specified by 'new_name' variable
	*) rename_copy_func && mv $scr_tmp_dir/$scr_file $scr_dir/$new_name.png && 
	notify-send 'Scrot' ''$new_name'.png has been saved.' ;;

esac

# echo $new_name

# Clean the temporary directory
rm -rf $scr_tmp_dir
