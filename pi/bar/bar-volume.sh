#!/bin/bash

volume=$(pulsemixer '--get-volume' | cut -c -3)

if (( volume > 50 ))
then
	echo -e "\uE05d$volume"
elif (( volume = 50 ))
then
	echo -e "\uE050$volume"
elif (( volume < 50 && volume >= 10 ))
then
	echo -e "\uE04e$volume"
else
	echo -e "\uE04f$volume"
fi
