#!/bin/bash

gpu_temp=$(vcgencmd measure_temp | cut -c 6- | cut -c -2)
cpu_temp=$(cat /sys/class/thermal/thermal_zone0/temp | cut -c -2)

array=($gpu_temp $cpu_temp)
IFS=$'\n'

# Pick up the highest value
res() {
	echo "${array[*]}" | sort -nr | head -n1
}

echo -e "\uE01C$(res)°C"
