#!/bin/bash

# Arch Linux Base Packages Installation Script
# http://www.ctrl-c.club/~pipe/archguide/stage-3.html#install-using-the-script.html

# Colors for messages
cb_white='\033[1;37m'
cb_green='\033[1;32m'
c_yellow='\033[0;33m'

# No color
nc='\033[0m'

# Prompts
microcode_prompt() {
	p_micro_intel="intel-ucode"
	p_micro_amd="amd-ucode"

	while true; do
		echo -e "${cb_white}[2/6] Choose the CPU microcode firmware according to your manufacturer:${nc}\n1. intel-ucode\n2. amd-ucode\n"
		read -p "Your choice: " microcode

		case $microcode in
			"1") echo -e "${cb_green}$p_micro_intel selected.${nc}\n";
				 microcode_choice=$p_micro_intel;
				 break ;;
			"2") echo -e "${cb_green}$p_micro_amd selected.${nc}\n";
				 microcode_choice=$p_micro_amd;
				 break ;;
			*) echo -e "${c_yellow}Please enter the number of your choice.${nc}\n";;
		esac
	done
}

kernel_prompt() {
	p_kernel="linux"
	p_kernel_lts="linux-lts"

	while true; do
		echo -e "${cb_white}[3/6] Choose the kernel:${nc}\n1. $p_kernel - The latest stable release of Linux kernel and its modules\n2. $p_kernel_lts - The Long-term support Linux kernel and its modules\n"
		echo -e "${cb_green}TIP:${nc} For new users a suggestion is to choose 'linux-lts'. You can always switch\nto 'linux' or vise-versa later. Both kernels updating on a regular basis.\n"
		read -p "Your choice: " kernel

		case $kernel in
			"1") echo -e "${cb_green}$p_kernel selected.${nc}\n";
				 kernel_choice=$p_kernel;
				 break ;;
			"2") echo -e "${cb_green}$p_kernel_lts selected.${nc}\n";
				 kernel_choice=$p_kernel_lts;
				 break ;;
			*) echo -e "${c_yellow}Please enter the number of your choice.${nc}\n";;
		esac
	done
}

permission_tool_prompt() {
	p_sudo="sudo"
	p_doas="doas"

	while true; do
		echo -e "${cb_white}[4/6] Choose the permission elevation tool:${nc}\n1. $p_sudo - Gives users the ability to run commands as root\n2. $p_doas - Simplified and lightweight alternative to 'sudo'\n"
		echo -e "${cb_green}TIP:${nc} For new users a suggestion is to choose 'sudo'.\n"
		read -p "Your choice: " permission_tool

		case $permission_tool in
			"1") echo -e "${cb_green}$p_sudo selected.${nc}\n";
				 permission_choice=$p_sudo;
				 break ;;
			"2") echo -e "${cb_green}$p_doas selected.${nc}\n";
				 permission_choice=$p_doas;
				 break ;;
			*) echo -e "${c_yellow}Please enter the number of your choice.${nc}\n";;
		esac
	done
}

bluetooth_prompt() {
	p_bluetooth="bluez bluez-utils pulseaudio-bluetooth"
	p_no_bluetooth=""

	while true; do
		echo -e "${cb_white}[5/6] Do you wish to install bluetooth utilities?${nc}\n"
		echo -e "- bluez - Daemons for the Bluetooth protocol stack\n- bluez-utils - Bluetooth management utilities\n- pulseaudio-bluetooth\n"
		read -p "Your choice: [Y/n] " bluetooth_yn

		case $bluetooth_yn in
			[Yy]*) echo -e "${cb_green}Bluetooth utilities selected.${nc}\n";
				bluetooth_choice=$p_bluetooth;
				bluetooth_yn_choice="Yes"
				break;;
			[Nn]*) echo -e "${cb_green}No bluetooth utilities selected.${nc}\n";
				bluetooth_choice=$p_no_bluetooth;
				bluetooth_yn_choice="No"
				break;;
			*) echo -e "${c_yellow}Please answer yes or no.${nc}\n";;
		esac
	done
}

base_dependencies="cryptsetup device-mapper e2fsprogs less"

# Install inderect dependencies of the 'base' package first
echo -e "${cb_white}[1/5] Installing inderect dependencies of the 'base' package...${nc}\n"
echo -e "$base_dependencies\n"

pacstrap /mnt $base_dependencies

# Ask for user specific packages
microcode_prompt
kernel_prompt
permission_tool_prompt
bluetooth_prompt

base_packages="base s-nail $microcode_choice $kernel_choice linux-firmware jfsutils reiserfsprogs sysfsutils xfsprogs htop logrotate mdadm which $permission_choice ufw lvm2 udisks2 $bluetooth_choice usbutils links lynx screen nano vi vim man-db man-pages texinfo connman dhcpcd inetutils iproute2 iw openssh openvpn wpa_supplicant diffutils gcc make pkgconf pulseaudio pulseaudio-alsa perl python python-pip"

install_prompt() {
	while true; do
		echo -e "${cb_white}[6/6] The following packages will be installed:${nc}\n"

		echo -e "${cb_green}Selected packages:${nc}\n"
		echo -e "Microcode:           $microcode_choice"
		echo -e "Kernel:              $kernel_choice"
		echo -e "Permission tool:     $permission_choice"
		echo -e "Bluetooth utilities: $bluetooth_yn_choice\n"

		echo -e "${cb_green}Final list:${nc}\n"
		echo -e "$base_packages\n"
		read -p "Continue?: [Y/n] " install_yn

		case $install_yn in
			[Yy]*) echo -e "${cb_green}Proceeding to the installation...${nc}\n";
				pacstrap /mnt $base_packages;
				break;;
			[Nn]*) echo -e "${c_yellow}Installation aborted.${nc}\n";
				break;;
			*) echo -e "${c_yellow}Please answer yes or no.${nc}\n";;
		esac
	done
}

install_prompt
